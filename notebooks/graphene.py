import numpy as np
import scipy.special as spc
####################################################################
def graphene_f(kx,ky,a):
    '''Function appearing in NN tight-binding model of graphene'''
    return np.exp(1j*a*kx)+np.exp(-1j*a*kx/2.)*2.*np.cos(np.sqrt(3.)/2.*a*ky)
######################################################################
def Z0(eps):
    '''Auxiliary piecewise function needed to calculate graphene DOS in TB model. See Eq. 14 in DOI: 10.1103/RevModPhys.81.109'''
    return np.piecewise(eps,[abs(eps)<1,(1<=abs(eps)) & (abs(eps)<3),abs(eps)>=3],[lambda eps:(1.+abs(eps))**2-(eps**2-1)**2/4.,
                                                                  lambda eps:4.*abs(eps),
                                                                  lambda eps:12.])
######################################################################
def Z1(eps):
    '''Auxiliary piecewise function needed to calculate graphene DOS in TB model. See Eq. 14 in DOI: 10.1103/RevModPhys.81.109'''
    return np.piecewise(eps,[abs(eps)<1,(1<=abs(eps)) & (abs(eps)<3),abs(eps)>=3],[lambda eps:4.*abs(eps),
                                                                    lambda eps:(1.+abs(eps))**2-(eps**2-1)**2/4.,
                                                                    lambda eps:0.])
######################################################################
def dos_graphene(epsilon,t,a):
    #in units of 1/t epsilon in units of t
    '''DOS of graphene in the NN tight-binding approximation. See Eq. 14 in DOI: 10.1103/RevModPhys.81.109'''
    eps=epsilon/t
    OMEGA=1.5*np.sqrt(3.)*a**2
    return 1./(t*OMEGA)*(0.25*(1.-np.sign(eps-3.))*(1.+np.sign(eps+3.)))*4./(np.pi**2*np.sqrt(Z0(eps)))*abs(eps)*spc.ellipk(Z1(eps)/Z0(eps))
#######################################################################
def dos_graphene_dirac(epsilon,t,a):
    '''DOS of graphene in the Dirac-cone approximation.'''
    return 8./(9.*np.pi*t**2*a**2)*abs(epsilon)
#######################################################################
def sigma_intraband_graphene(omega, Ef, eta=0.):
    '''graphene intraband conductivity in units of conductance quantum 2e^2/h, all parameters in eV'''
    if eta ==0.:
        return 1.j*abs(Ef)/omega
    else:
        return abs(Ef)*(eta+1j*omega)/(omega**2+eta**2)
def sigma_interband_graphene(omega, Ef, eta=0.):
    '''graphene interband conductivity in units of conductance quantum 2e^2/h, all parameters in eV'''
    if eta ==0.:
        return 0.25*np.pi*(0.5*(np.sign(omega-2*abs(Ef))+1.)+0.5*(np.sign(-omega-2*abs(Ef))+1.)
                 +1j/np.pi*np.log(abs((2.*abs(Ef)-omega)/(2.*abs(Ef)+omega))) )
    else:
        return np.pi*(0.25*(0.5+1./np.pi*np.arctan((omega-2.*abs(Ef))/(2.*abs(Ef)*eta)))+
                 0.25*(0.5+1./np.pi*np.arctan((-omega-2.*abs(Ef))/(2.*abs(Ef)*eta)))+
                 1j/(8.*np.pi)*np.log(((2.*abs(Ef)-omega)**2+eta**2)/((2.*abs(Ef)+omega)**2+eta**2)))

def Re_sigma_interband_Stauber(omega,mu,T,t,a):
    return 9.*np.pi**2*t**2*a**2/(32*omega)*dos_graphene(omega/2.,t,a)*(1.-0.5*(omega/(3*t))**2)*(np.tanh((omega+2*mu)/(4.*T))+np.tanh((omega-2*mu)/(4.*T)))

def G(z):
    z = z+0.j
    sz2m1 = np.sqrt(z-1.)*np.sqrt(z+1.)
    return z*sz2m1 - np.log(z + sz2m1)

def chi(omega, q, Ef):
    hv_F = 0.658 # \hbar v_{\rm F} in eV nm
    k_F = np.abs(Ef/hv_F)
    Dplus  = (omega/hv_F + 2.*k_F)/q
    Dminus = (omega/hv_F - 2.*k_F)/q

    it1 = 8.*k_F/(hv_F*q*q)
    it2 = (np.where(np.real(Dminus) < -1.,
                    G(-Dminus), # low w and low k
                    G(Dminus) + 1.j*np.pi  # high w or high k
                    )
           - G(Dplus)
          ) / np.sqrt(0.j + omega*omega - hv_F*hv_F*q*q)

    return -0.25/np.pi*(it1+it2)*q*q